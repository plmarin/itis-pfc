// -----------------------------------------------------------------------------
/**
 *  @brief AllFacesTracker implementation
 *  @author Jose M. Buenaposada
 *  @date 2009/13/10
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ----------------------- INCLUDES --------------------------------------------
#include "all_faces_tracker.hpp"
#include <algorithm> // std::copy
#include <limits>
#include <set>
#include "trace.hpp"

namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
//
// Purpose and Method: Constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
AllFacesTracker::AllFacesTracker
  (
  CvHaarClassifierCascade* pCascade,
  CvHaarClassifierCascade* pCascadeLeftEye,
  CvHaarClassifierCascade* pCascadeRightEye,
  double scale_step,
  double min_neighbors,
  double min_face_size,
  bool draw_eyes
  ):
  m_scale_step(scale_step),
  m_min_neighbors(min_neighbors),
  m_min_face_size(min_face_size),
  m_eyes_detector(pCascadeLeftEye, pCascadeRightEye, pCascade->orig_window_size.width),
  m_draw_eyes(draw_eyes),
  m_num_faces_created(0)
{
  if (pCascade != NULL)
  {
    m_pStorage = cvCreateMemStorage(0);
    m_pCascade = pCascade;
  }
 
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: Destructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
AllFacesTracker::~AllFacesTracker
  ()
{
  if (m_pStorage) 
  {
    cvReleaseMemStorage(&m_pStorage);
  }

  if (m_pCascade) 
  {
    cvReleaseHaarClassifierCascade(&m_pCascade);
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
AllFacesTracker::processFrame 
  (
  IplImage *pImage,
  Faces& tracked_faces
  )
{
  IplImage* pGrayImage;
  IplImage* pAuxImage;
  CvSeq* faces;
  CvPoint left_eye, right_eye;
  CvPoint nose, mouth;
  int max_width = 0;
  int max_height = 0;
  int nearest_face_index = -1;
  static float alpha = 0.5;
  static float alpha1 = 1.0 - alpha;
  double ratio_height, ratio_width, ratio_x, ratio_y;
  double min_ratio_height, min_ratio_width, min_ratio_x, min_ratio_y;
  int x, y, width, height;
  ImageRegion region;

  if ((!m_pCascade) || (!m_pStorage))
  {
    return;
  };
  
  // Obtain a grey levels version of the input image 
  pGrayImage = cvCreateImage( cvSize(pImage->width, pImage->height), 8, 1 );
  if ( pImage->origin == IPL_ORIGIN_TL )
  {
    cvCvtColor( pImage, pGrayImage, CV_BGR2GRAY );
  }
  else
  {
    pAuxImage  = cvCreateImage( cvSize(pImage->width, pImage->height), 8, 1 );
    cvCvtColor( pImage, pAuxImage, CV_BGR2GRAY );
    cvFlip( pAuxImage, pGrayImage, 0 );
  }

  // Prepare the face detection memory storage and detect faces
  cvClearMemStorage( m_pStorage );

  faces = cvHaarDetectObjects(pGrayImage, m_pCascade, m_pStorage,
                              m_scale_step,
                              m_min_neighbors, 
                              CV_HAAR_DO_CANNY_PRUNING | CV_HAAR_SCALE_IMAGE,
                              //CV_HAAR_DO_CANNY_PRUNING,
                              cvSize(m_min_face_size, m_min_face_size));
  cvReleaseImage(&pGrayImage);
  
  if (faces->total > 0)
  {
    // We have found at least one face 
    if (tracked_faces.empty())
    {
      nearest_face_index = -1;
 
      for( int i = 0; i < (faces ? faces->total : 0); i++ )
      {
        CvRect* r = (CvRect*)cvGetSeqElem( faces, i );
        region.setParams(r->x, r->y, r->width, r->height);
            
//        SHOW_VALUE(m_num_faces_created);
        boost::shared_ptr<Face> face_ptr(new Face(++m_num_faces_created));
        face_ptr->setRegion(region);      
        m_eyes_detector.detect(pImage, region, left_eye, right_eye);
        face_ptr->setEyesRelativeCoordinates(left_eye, right_eye);
        tracked_faces.push_back(face_ptr);      
      };      
    }    
    else
    {
      // Look for the nearest detected faces to already known 
      // ones (in tracked_faces)
      Faces new_tracked_faces;
      ImageRegion old_region;
      int nearest_index;
      std::set<int> known_faces_indices;

      for( int i = 0; i < (faces ? faces->total : 0); i++ )
      {
        CvRect* r = (CvRect*)cvGetSeqElem( faces, i );
        region.setParams(r->x, r->y, r->width, r->height);

        // Look for the closest tracked face to the detected one
        min_ratio_height = std::numeric_limits<int>::max();
        min_ratio_width  = std::numeric_limits<int>::max();
        min_ratio_x      = std::numeric_limits<int>::max();
        min_ratio_y      = std::numeric_limits<int>::max();      
	nearest_index    = -1;
        for( int j= 0; j < tracked_faces.size(); j++)
	{
          tracked_faces[j]->getRegion(old_region);
          old_region.getParams(x, y, width, height);      

          ratio_height = static_cast<double>(abs(r->height-height))/static_cast<double>(height);
          ratio_width  = static_cast<double>(abs(r->width-width))/static_cast<double>(width);
/*        ratio_x      = static_cast<double>(abs(r->x+(static_cast<double>(r->width)/2.0)-x-(static_cast<double>(width)/2.0)))/static_cast<double>(width);
          ratio_y      = static_cast<double>(abs(r->y+(static_cast<double>(r->height)/2.0)-y-(static_cast<double>(height)/2.0)))/static_cast<double>(height);*/
          ratio_x      = static_cast<double>(abs(r->x-x))/static_cast<double>(width);
          ratio_y      = static_cast<double>(abs(r->y-y))/static_cast<double>(height);
          if ( (ratio_width  < min_ratio_width) && 
               (ratio_height < min_ratio_height)  && 
               (ratio_x      < min_ratio_x)  &&
               (ratio_y      < min_ratio_y) )
          {
            nearest_index    = j;
            min_ratio_height = ratio_height;
            min_ratio_width  = ratio_width;
            min_ratio_x      = ratio_x;
            min_ratio_y      = ratio_y;
          }
        }

  
        //if ( (min_ratio_height <= 0.3) && (min_ratio_width <= 0.3) &&
	//     (min_ratio_x <= 0.2) && (min_ratio_y <= 0.2) )
        
        if ( (min_ratio_x <= 0.3) && (min_ratio_y <= 0.3) )
	{
          // Update the new face position on image
          tracked_faces[nearest_index]->getRegion(old_region);
          old_region.getParams(x, y, width, height);      
          region.setParams(static_cast<int>((alpha*static_cast<double>(x)) +
                                            (alpha1*static_cast<double>(r->x))), 
                           static_cast<int>((alpha*static_cast<double>(y)) +
                                            (alpha1*static_cast<double>(r->y))), 
                           static_cast<int>((alpha*static_cast<double>(width)) + 
                                            (alpha1*static_cast<double>(r->width))), 
                           static_cast<int>((alpha*static_cast<double>(height)) + 
                                            (alpha1*static_cast<double>(r->height))));
//          region.setParams(r->x, r->y, r->width, r->height);
          tracked_faces[nearest_index]->setRegion(region); 
          known_faces_indices.insert(nearest_index);
	}   
	else
	{
	  // The detected face is new
          boost::shared_ptr<Face> face_ptr(new Face(++m_num_faces_created));
          face_ptr->setRegion(region);      
          new_tracked_faces.push_back(face_ptr);      	  
	}
        
      };

      // Remove not re-detected faces from tracked_faces vector
      Faces::iterator it;
      std::set<int>::iterator it_set;
      int face_index = 0;

      for(it = tracked_faces.begin(); it != tracked_faces.end(); )
      {  
        it_set = known_faces_indices.find(face_index);
        if (it_set != known_faces_indices.end())
        {
          // We find here the eyes of the detected face
          (*it)->getRegion(region);
          m_eyes_detector.detect(pImage, region, left_eye, right_eye);
          (*it)->setEyesRelativeCoordinates(left_eye, right_eye);

          // Go for next face
          ++it; 
        }
        else    
        {
           it = tracked_faces.erase(it);  
        }
        face_index++;
      }

      // Insert the new detected faces on the tracked_faces vector
      for(it = new_tracked_faces.begin(); it != new_tracked_faces.end(); it++)
      {
        // We find here the eyes of the detected face
        (*it)->getRegion(region);
        m_eyes_detector.detect(pImage, region, left_eye, right_eye);
        (*it)->setEyesRelativeCoordinates(left_eye, right_eye);

        tracked_faces.push_back(*it);
      }
    }
  }
  else
  {
    // We have not found any face
    tracked_faces.clear();
  }
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
AllFacesTracker::showResults
  (
  Viewer* pViewer,
  IplImage *pImage,
  Faces& tracked_faces
  )
{
  int x, y;
  int width, height;
  CvPoint left_eye, right_eye;
  CvPoint nose, mouth;
  float red[3]   = {1.0, 0.0, 0.0};
  float green[3] = {0.0, 1.0, 0.0};
  float blue[3]  = {0.0, 0.0, 1.0};
  float white[3] = {1.0, 1.0, 1.0};
  float black[3] = {0.0, 0.0, 0.0};

  // Show detected faces.
  for (int i=0; i<tracked_faces.size(); i++)
  {
    ImageRegion region;
    tracked_faces[i]->getRegion(region); 
    region.getParams(x, y, width, height);
    pViewer->rectangle(x, 
                       y,
                       width,
                       height,
                       3,
                       white);
    // Draw face id.
/*
    pViewer->filled_rectangle(x+1, y+1, 
                              round(static_cast<float>(width)*0.3), 
                              round(static_cast<float>(height)*0.2), 
                              black);
    std::ostringstream outs;  // Declare an output string stream.
    outs << tracked_faces[i]->getFaceId() << std::ends;
    std::string face_id = outs.str();   // Get the created string from the output stream.
    pViewer->text(face_id, x, y + round(static_cast<float>(height)*0.15), 
                  white, 
                  static_cast<float>(width)/200.0);
*/
    // Draw eyes
    tracked_faces[i]->getEyesRelativeCoordinates(left_eye, right_eye);
    if (m_draw_eyes)
    {
      m_eyes_detector.showResults(pViewer, pImage, region, left_eye, right_eye);
    }
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
/*
void
AllFacesTracker::getTrackedFaces
  (
  upm::pcr::Faces& tracked_faces
  )
{
  tracked_faces.resize(m_tracked_faces.size());
  std::copy(m_tracked_faces.begin(), m_tracked_faces.end(), tracked_faces.begin());
};
*/

}; }; // namespace
