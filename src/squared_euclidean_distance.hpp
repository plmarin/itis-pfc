// -----------------------------------------------------------------------------
/**
 *  @brief Squared eucludean distance
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef SQUARED_EUCLIDEAN_DISTANCE_HPP
#define SQUARED_EUCLIDEAN_DISTANCE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class SquaredEuclideanDistance
 * @brief Computes the square of the euclidean distance 
 */
// -----------------------------------------------------------------------------
class SquaredEuclideanDistance
{
public:
  SquaredEuclideanDistance
    ();

  virtual 
  ~SquaredEuclideanDistance
    ();

  /**
   * @brief Get the distance squared euclidean distance
   *
   * @param descriptor1 
   * @param descriptor2 
   */
  virtual double
  computeDistance
   (
   CvMat* d1,
   CvMat* d2
   );
   
   /**
   * @brief Get the distance squared euclidean distance
   *
   * @param descriptor1 
   * @param descriptor2 
   */
  virtual double
  computeDistance
   (
   cv::Mat d1,
   cv::Mat d2
   );
};

}; }; // namespace

#endif

