// -----------------------------------------------------------------------------
/**
 *  @brief Face processors group interface definition
 *  @author Jose M. Buenaposada
 *  @date 2011/01/29
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_PROCESSORS_GROUP_HPP
#define FACE_PROCESSORS_GROUP_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_processor.hpp"

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class FaceProcessorsGroup
 *
 * @brief Defines the interface for a group of Face processors.
 *
 * We follow a Composite design pattern and Prototype pattern (clone()).
 */
// -----------------------------------------------------------------------------
class FaceProcessorsGroup: public FaceProcessor
{
public:

  FaceProcessorsGroup
    ();

  /** Copy constructor */
  FaceProcessorsGroup
    (
    const FaceProcessorsGroup& group
    );

  /** Asignment operator  */
  FaceProcessorsGroup&
  operator =
    (
    const FaceProcessorsGroup& group
    );

  virtual 
  ~FaceProcessorsGroup
    () {};

  /**
   * @brief Process a Face.
   *
   * @param  pFrame Image comming from the camera.
   * @param  f The Face pFrame to process.
   */
  virtual void
  processFrame 
   (
   IplImage* pFrame,
   Face& f 
   );

  /**
   * @brief Shows the result of Face processing on a given Viewer.
   *
   * @param pViewer The viewer over to which display results.
   * @param pFrame Last processed camera frame.
   * @param f The last Face passed in the last call to "processFrame".
   */
  virtual void
  showResults
   (
   Viewer* pViewer,
   IplImage* pFrame,
   Face& f
   );

  /**
   * @brief Returns a pointer to a copy of itself.
   */
  virtual FaceProcessorPtr
  clone
   ();

  void
  addProcessor
    (
    FaceProcessorPtr processor_ptr
    );

  size_t
  getNumberOfProcessors
    () { return m_processors.size(); };

  /**
   * @brief Get the face attributes computed by the Face processor.
   *
   * @param attributes
   */
   virtual void
   getAttributes
     (
     FaceAttributes& attributes
     );

private:
  FaceProcessors m_processors;

};

}; }; // namespace

#endif
