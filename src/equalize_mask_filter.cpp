// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of face age regressor image region processor.
 *  @author Jose M. Buenaposada
 *  @date 2010/12/02
 *  @version $revision
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "equalize_mask_filter.hpp"
#include <opencv/cv.h>
#include "trace.hpp"
#include "similarity_warp_filter.hpp"

namespace upm { namespace pcr {

#define NORM_LEFT_EYE_X 7
#define NORM_LEFT_EYE_Y 10
#define NORM_RIGHT_EYE_X 16 //18
#define NORM_RIGHT_EYE_Y 10
#define NORM_FACE_SIZE 25

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EqualizeMaskFilter::EqualizeMaskFilter 
  ( 
  CvMat* pMask,
  int img_width,
  int img_height,
  bool use_eyes_location, /* = false */
  bool use_equalization /* = true */
  ):
  m_img_width(img_width), 
  m_img_height(img_height)
{
  double scale;
  
  m_pMask = NULL;
  if (pMask != NULL)
  {
    assert(img_width = pMask->cols);
    assert(img_height = pMask->rows);
    m_pMask = cvCloneMat(pMask);
  }
  
  m_use_eyes_location = use_eyes_location;
  m_use_equalization  = use_equalization;
  scale                = (static_cast<double>(img_width)/static_cast<double>(NORM_FACE_SIZE));
  m_norm_left_eye.x    = cvRound(NORM_LEFT_EYE_X*scale);
  m_norm_left_eye.y    = cvRound(NORM_LEFT_EYE_Y*scale);
  m_norm_right_eye.x   = cvRound(NORM_RIGHT_EYE_X*scale);
  m_norm_right_eye.y   = cvRound(NORM_RIGHT_EYE_Y*scale);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EqualizeMaskFilter::EqualizeMaskFilter 
( 
  const EqualizeMaskFilter& filter 
) 
{
  m_pMask = NULL;
  (*this) = (filter);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EqualizeMaskFilter& 
EqualizeMaskFilter::operator= 
  ( 
  const EqualizeMaskFilter& filter 
  )
{
  if (&filter == this)
  {
    return (*this);
  };

  if (m_pMask != NULL)
  {
    cvReleaseMat(&m_pMask); 
  }
  m_pMask = NULL;
  
  if (filter.m_pMask != NULL)
    m_pMask = cvCloneMat(filter.m_pMask);
  
  m_use_eyes_location = filter.m_use_eyes_location;
  m_use_equalization  = filter.m_use_equalization;
  m_img_width         = filter.m_img_width;
  m_img_height        = filter.m_img_height;
  m_norm_left_eye.x   = filter.m_norm_left_eye.x;
  m_norm_left_eye.y   = filter.m_norm_left_eye.y;
  m_norm_right_eye.x  = filter.m_norm_right_eye.x;
  m_norm_right_eye.y  = filter.m_norm_right_eye.y;

  return (*this);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EqualizeMaskFilter::~EqualizeMaskFilter()
{
  if (m_pMask != NULL)
  {
    cvReleaseMat(&m_pMask);
  }
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
EqualizeMaskFilterPtr
EqualizeMaskFilter::clone
  ()
{
  EqualizeMaskFilterPtr filter_ptr(new EqualizeMaskFilter((*this)));

  return filter_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
EqualizeMaskFilter::operator() 
  ( 
  IplImage* pFrame, 
  Face& f,
  IplImage* pFilteredImage
  )
{
  int x, y;
  int kkkk;
  int width, height;
  IplImage* pAuxImage;
  IplImage* pGrayImage;
  ImageRegion region;
  CvPoint left_eye, right_eye;
  double dist_src, dist_dst;
  double scale;
  double final_angle, init_angle;
  
  
  assert(pFrame != NULL);
//  assert(m_pMask != NULL);
  
  assert(pFilteredImage != NULL);
  
//   if (pFilteredImage == NULL)
//   {
//     pFilteredImage = cvCreateImage(cvSize(m_img_width, 
//                                           m_img_height),
//                                    IPL_DEPTH_8U, 1);
//   }

  // Crop face area from pFrame image and resize it to the size of the pFaceImage.
  // If we have detected both eyes we use its location to make a similarity
  // transform.
  f.getRegion(region);
  region.getParams(x, y, width, height);
  f.getEyesRelativeCoordinates(left_eye, right_eye);
  
  if ( m_use_eyes_location &&
       (left_eye.x  != -1) && (left_eye.y  != -1) &&
       (right_eye.x != -1) && (right_eye.y != -1))
  {
    SimilarityWarpFilter warpFilter(left_eye, right_eye, 
                                    m_norm_left_eye, m_norm_right_eye,
                                    cvSize(m_img_width, m_img_height));

    pAuxImage = cvCreateImage(cvSize(m_img_width, m_img_height), 
			      pFrame->depth, pFrame->nChannels);
    warpFilter( pFrame, f, pAuxImage );
  
    assert(pAuxImage->width  == m_img_width);
    assert(pAuxImage->height == m_img_height);
  }
  else
  {
    cvSetImageROI(pFrame, cvRect(x,y,width,height));
    pAuxImage  = cvCreateImage(cvSize(m_img_width,
                                      m_img_height),
                              pFrame->depth, pFrame->nChannels);
    cvResize(pFrame, pAuxImage, CV_INTER_LINEAR );
    cvResetImageROI(pFrame);
  };
  
  // Convert the cropped image to gray scale and equalize the histogram
  if (pFrame->nChannels == 3)
  {
    if (m_use_equalization)
    {
      pGrayImage = cvCreateImage(cvSize(m_img_width,
                                      m_img_height), 
                                 IPL_DEPTH_8U, 1);
      cvCvtColor( pAuxImage, pGrayImage, CV_BGR2GRAY );
      cvEqualizeHist( pGrayImage, pFilteredImage );
      cvReleaseImage(&pGrayImage);
    }
    else
    {
      cvCvtColor( pAuxImage, pFilteredImage, CV_BGR2GRAY );
    }
  }
  else
  {
    if (m_use_equalization)
    {
      cvEqualizeHist( pAuxImage, pFilteredImage );
    }
    else
    {
      cvCopy(pAuxImage, pFilteredImage);
    }
  }

  // Put the mask over the image
  if (m_pMask != NULL)
    putMask(pFilteredImage, m_pMask);

  // release memory
  cvReleaseImage(&pAuxImage);
};

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
EqualizeMaskFilter::putMask
(
  IplImage* pImage,
  CvMat* pMask
)
{
  double old_value;
  double new_value;

  assert(pImage->depth == IPL_DEPTH_8U);
  assert(pImage->nChannels == 1);
  assert(pMask != NULL);

  for (int i=0; i < pImage->height; i++)
  {
    for (int j=0; j < pImage->width; j++)
    {
      old_value = static_cast<double>(((uchar *)(pImage->imageData +
                                                 i*pImage->widthStep))[j]);
      new_value = old_value * static_cast<double>(cvmGet(pMask, i, j));
      ((uchar *)(pImage->imageData +
                 i*pImage->widthStep))[j] = static_cast<uchar>(new_value);
    }
  }
};

}; }; // namespaces
