// -----------------------------------------------------------------------------
/**
 *  @brief Face recognition Data Base class
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_ID_DATA_BASE_HPP
#define FACE_ID_DATA_BASE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <queue>
#include <limits>
#include "distance.hpp"
#include "viewer.hpp"
#include <boost/shared_ptr.hpp>

namespace upm { namespace pcr
{

class FaceIdDataBase;
typedef boost::shared_ptr<FaceIdDataBase> FaceIdDataBasePtr;    
  
// ----------------------------------------------------------------------------
/**
 * @class FaceIdDataBase
 * @brief Face Descriptors Data Base and face recogniser
 * 
 * In this class we compare descriptors using a Distance subclass object. 
 * The distance between descriptors once computed is used to 
 */
// -----------------------------------------------------------------------------
class FaceIdDataBase
{
public:
  /** This is the type for face identifiers */
  typedef unsigned int id_type; 
  static const unsigned int UNKNOWN_ID;
  static const unsigned int MAX_ID;
  
  /** Static (class) method to convert a face identifier to std::string */
  static std::string faceIdToString(id_type face_id);
  
  FaceIdDataBase
    ( 
    DistancePtr distance
    );

  ~FaceIdDataBase
    ();

  /**
   * @brief Process the descriptors of a face for recognition.
   *
   * @param descriptors The queue of descriptors from a face.
   */
  id_type
  getID
   (
   std::queue<cv::Mat>& descriptors,
   cv::Mat representative_image
   );

  void
  showDataBaseImages
    (
    Viewer* pViewer,
    int images_per_row = 5
    );

private:
  DistancePtr m_distance;
  std::vector<std::queue<cv::Mat> > m_dataBase;
  std::vector<cv::Mat> m_images; // We store a representative image per individual in the database.

  const double m_same_prior;
  const double m_diff_prior;
  const double m_max_prob_same_for_new;
  const double m_diff_prob_same_for_unknown;
  
  double 
  findDescriptorSimilarityToSubject
  (
  int db_subject, 
  cv::Mat descriptor    
  );

  double 
  normpdf_similarity
    (
    double x,
    double& prob_same, 
    double& prob_diff
    );

  double
  normpdf_same
    (
    double x
    );
    
  double
  normpdf_diff
    (
    double x
    );
};

}; }; // namespace

#endif

