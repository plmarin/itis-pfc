// -----------------------------------------------------------------------------
/**
 *  @brief Face Background Model to compute Distances to faces in a Fixed DB.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "face_background_model.hpp"

namespace upm { namespace pcr
{

//#include "pie.hpp"
// #include "pie_lda_pca.hpp"
#include "pie_pca_lda_features.h"

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceBackgroundModel::FaceBackgroundModel
  ()
{
  cv::flann::KDTreeIndexParams kdtree_params;
  
  for (int s = 0; s < NUM_SUBJECTS; s++)
  {
    cv::Mat subjects_data(NUM_LDA_FEATURES, NUM_IMGS_PER_SUBJECT, cv::DataType<float>::type, PIE_LDA_PROJECTIONS[s]);    
    KdTreePtr kdtree_ptr(new cv::flann::Index(subjects_data, kdtree_params));
    m_subject_kdtrees.push_back(kdtree_ptr);  
  }
}
      
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------      
FaceBackgroundModel::~FaceBackgroundModel
  ()
{
}

// Comparison struct used by sort
// http://bytes.com/topic/c/answers/132045-sort-get-index
template<class T> struct index_cmp
{
  index_cmp(const T arr) : arr(arr) {}
  bool operator()(const size_t a, const size_t b) const
  {
    return arr[a] < arr[b];
  }
  const T arr;
};

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
cv::Mat
FaceBackgroundModel::computeHighLevelDescriptor
  (
  cv::Mat low_level_descriptor
  )
{ 
  int knn = 1;	// Number of nearest neighbors to search for
  std::vector<int> found_indices(knn);
  std::vector<float> found_dists(knn);
  std::vector<int> indices;
  std::vector<float> distances;
  
  std::vector<float> low_level_descriptor_vector;
  for(int i = 0; i < low_level_descriptor.rows; i++)  
  {
    low_level_descriptor_vector.push_back(low_level_descriptor.at<float>(i,0));
  }
  
  for (int s = 0; s < NUM_SUBJECTS; s++)
  {
    // Use L2 (euclidean) by default in the Kd-Tree as Low-level descriptors 
    // comparation: the lower the distance the most similar they are.
    m_subject_kdtrees[s]->knnSearch(low_level_descriptor_vector,
				   found_indices, 
				   found_dists, knn, 
				   cv::flann::SearchParams(64));
    
    distances.push_back(found_dists.front());
    indices.push_back(s);
  };
  
  // Obtain the vector on indices ordered from smallest to highest distances
  std::sort(indices.begin(), indices.end(), index_cmp<std::vector<float>& >(distances));
  
  return cv::Mat(calcularTau(indices),true); // copy data from std::vector.
}

std::vector<int>
FaceBackgroundModel::calcularTau(std::vector<int>& indices)
{
  std::vector<int> tau(indices.size());
  
  for(int i = 0; i < indices.size(); i++)
  {
    tau.at(indices.at(i)) = i;
  }
  
  return tau;
}

  
}; }; // namespace

