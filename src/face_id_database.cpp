// -----------------------------------------------------------------------------
/**
 *  @brief Face recognition Data Base class implementation
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "face_id_database.hpp"
#include <cmath>
#include <limits>
#include <boost/concept_check.hpp>
#include "trace.hpp"

namespace upm { namespace pcr
{
  
// #include "character_distribution.hpp"
#include "rank_of_similarities_classifier.h"

const unsigned int FaceIdDataBase::UNKNOWN_ID = std::numeric_limits<unsigned int>::max();
const unsigned int FaceIdDataBase::MAX_ID     = std::numeric_limits<unsigned int>::max() - 1;


// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
std::string 
FaceIdDataBase::faceIdToString
  (
  FaceIdDataBase::id_type face_id
  )
{
  std::ostringstream outs;  
  if (face_id == UNKNOWN_ID)  
  { 
    outs << "???" << std::ends;
  }
  else
  {
    outs << face_id << std::ends;
  }
  return outs.str(); 
}
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdDataBase::FaceIdDataBase
  ( 
  DistancePtr distance
  ):
  m_distance(distance),
  m_same_prior(0.5),
  m_diff_prior(0.5),
  m_max_prob_same_for_new(0.4),
  m_diff_prob_same_for_unknown(0.2)
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdDataBase::~FaceIdDataBase
  ()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
//   1) Para todo descriptor en "descriptors" calcular la mínima distancia (que será la similitud
//   de permutaciones en nuestro caso) con cada uno de las imágenes de todos los índividuos en 
//   la base de datos. Llamaremos distancia_i_j a la (mínima) distancia del descriptor j de entrada 
//   al individuo i.
//   2) Con la distancia mínima a cada individuo "i" en la base de datos (caras que ya hemos visto hace
//   un rato), calcular la P(distancia_i_j|"=") y P(distancia_i_j|"!=").
//   3) Con las dos probabilidades anteriores para cada individuo, podemos calcular la probabilidad 
//   de ser igual o distinto al individuo i: 
//
//    P("=", i| distancia_i_1, ... , distancia_i_n) = P(distancia_i_1|"=") * ... * P(distancia_i_n|"=") * P("=")
//    P("!=", i| distancia_i_1, ... , distancia_i_n) = P(distancia_i_1|"!=") * ... * P(distancia_i_n|"!=")*P("!=")
//
//   4) Declaramos la identidad conocida si P("=",i) >> P("!=",i) y P("=",i) es mucho mayor que la 
//   segunda mayor P("="|k) (donde k es el individuo correspondiente).
//
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdDataBase::id_type
FaceIdDataBase::getID
 (
 std::queue<cv::Mat>& descriptors,
 cv::Mat representative_image
 )
{
  double similarity; 
  double estimated_id;

  TRACE_INFO(" ==================== getID ================ " << std::endl);
  // 0) If we have an empty data base of individuals, the first one is just added.
  int size_dataBase    = m_dataBase.size();
  int size_descriptors = descriptors.size();
  if (size_dataBase == 0)
  {
    m_dataBase.push_back(descriptors);
    m_images.push_back(representative_image);
    return 0;
  }
  
  double prob_same[size_dataBase];
  double prob_diff[size_dataBase];
//   double ratio_same_diff[size_dataBase];
  double prob_same_id, prob_diff_id;
  
  // 1) Compute similarity of input sequence with all individuals (already known in DataBase).
  // 2) Compute the probability of being same (prob_same) and different (prob_diff) individuals 
  //    with all subjects already in the database.
  double max_     = std::numeric_limits<double>::min();
  int id_max_     = -1;
  double max_2nd  = std::numeric_limits<double>::min();
  for(int db_subject = 0; db_subject < size_dataBase; db_subject++)
  {
    prob_same[db_subject] = m_same_prior;
    prob_diff[db_subject] = m_diff_prior;
//     TRACE_INFO("----------- BEGIN ------------------" << std::endl);
    for(int i = 0; i < size_descriptors; i++)
    {
      cv::Mat descriptor1 = descriptors.front();  // Imagen "j", individuo nuevo
      descriptors.pop();      
      descriptors.push(descriptor1);
     
      similarity = findDescriptorSimilarityToSubject(db_subject, descriptor1);
//       TRACE_INFO("similarity(" << db_subject << "," << i << ")= " << similarity << std::endl);
//       SHOW_VALUE(normpdf_same(similarity));
//       SHOW_VALUE(normpdf_diff(similarity));
      normpdf_similarity(similarity, prob_same_id, prob_diff_id);
      prob_same[db_subject] *= prob_same_id;
      prob_diff[db_subject] *= prob_diff_id;      
    }
    TRACE_INFO("similarity = " << similarity << std::endl);
//     TRACE_INFO("------------- END ----------------" << std::endl);
    
    // Normalize to get probabilities (sum to 1)
    std::cout << "Before: prob_same[" <<  db_subject <<"] = " << prob_same[db_subject] << std::endl;
    std::cout << "Before: prob_diff[" << db_subject <<"] = " << prob_diff[db_subject] << std::endl;
    double sum                           = prob_same[db_subject] + prob_diff[db_subject];
    if (prob_same[db_subject] > prob_diff[db_subject])
    {
      prob_same[db_subject] /= sum;
      prob_diff[db_subject]  = 1.0 - prob_same[db_subject];
    }
    else
    {
      prob_diff[db_subject] /= sum;
      prob_same[db_subject]  = 1.0 - prob_diff[db_subject];    
    }
    std::cout << "After: prob_same[" <<  db_subject <<"] = " << prob_same[db_subject] << std::endl;
    std::cout << "After: prob_diff[" << db_subject <<"] = " << prob_diff[db_subject] << std::endl;

    if (prob_same[db_subject] > max_)
    {
       max_2nd = max_;
       max_    = prob_same[db_subject];
       id_max_ = db_subject;      
    }    
  }
      
  std::cout << "Biggest prob_same     = " << max_ << std::endl;
  std::cout << "2nd Biggest prob_same = " << max_2nd << std::endl;
  std::cout << "prob_same difference  = " << max_ - max_2nd << std::endl;
  
  if (max_ < m_max_prob_same_for_new ) // The max prob_same is small ==> New subject
  {
    std::cout << "NEW SUBJECT == : " << size_dataBase << std::endl;
    estimated_id = size_dataBase;
    m_dataBase.push_back(descriptors);
    m_images.push_back(representative_image);
  }
  else if ((max_ - max_2nd) < m_diff_prob_same_for_unknown) // undefined individual (is similar to at least two individuals in DataBase)
  {
    std::cout << "I'M NOT SURE ABOUT FACE" << std::endl;
    estimated_id = UNKNOWN_ID;    
  }
  else // It is similar to the subject in database.
  {
    std::cout << "KNOWN SUBJECT == : " << size_dataBase << std::endl;

    // We update the images of a known subject to "descriptors"
    estimated_id             = id_max_;
    m_dataBase[estimated_id] = descriptors;      
    m_images[estimated_id]   = representative_image;
  }
  
  std::cout << "DataBase Size: " << m_dataBase.size() << " (" << size_dataBase << ")" << std::endl;
  std::cout << "ID = " << estimated_id << std::endl;

  TRACE_INFO(" ===============***** FIN getID *****=========== " << std::endl);
 
  return estimated_id;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double 
FaceIdDataBase::findDescriptorSimilarityToSubject
  (
  int db_subject, 
  cv::Mat descriptor    
  )
{  
  std::queue<cv::Mat> db_descriptors = m_dataBase[db_subject]; 
  cv::Mat max_similarity_descriptor;
  
  double max_similarity = std::numeric_limits<double>::min();
  int max_index         = -1;  
//   TRACE_INFO("======" << std::endl);
  for (int d=0; d < db_descriptors.size(); d++)
  {
    cv::Mat descriptor_DB = db_descriptors.front();  // d-th descriptor of db_subject-th individual.
    db_descriptors.pop();
    db_descriptors.push(descriptor_DB);

//     TRACE_INFO("==========================" << std::endl);
//     SHOW_VALUE(descriptor);
//     SHOW_VALUE(descriptor_DB);

    // Find similarity of "descriptor" to the "db_subject-th" individual in the DB
    double similarity = 0;
    for (int j = 0; j < descriptor_DB.rows; j++)  
    {
      similarity += std::max(descriptor.rows - descriptor.at<int>(j,0), 0) *
                    std::max(descriptor_DB.rows - descriptor_DB.at<int>(j,0), 0);
    }
//     TRACE_INFO("similarity(" << db_subject << ", " << d << ") = " << similarity << std::endl);
    
    if (similarity > max_similarity) 
    {
      max_similarity = similarity;
      max_index      = d;      
      max_similarity_descriptor = descriptor_DB;
//       SHOW_VALUE(max_similarity);
//       SHOW_VALUE(max_index);
    }
  }
//   SHOW_VALUE(descriptor);
//   SHOW_VALUE(max_similarity_descriptor);
//   TRACE_INFO("======" << std::endl);
  
  return max_similarity;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdDataBase::showDataBaseImages
  (
  Viewer* pViewer,
  int images_per_row /* = 5 */
  )
{
  cv::Mat db_images;
  cv::Mat img;
  cv::Mat roi;

  if (m_images.size() == 0)
  {
    return; 
  }
  
  int num_rows  = ceil(static_cast<float>(m_images.size())/static_cast<float>(images_per_row));
  
  img           = m_images[0];
  db_images     = cv::Mat::zeros(img.rows*num_rows, img.cols*images_per_row, cv::DataType<uint8_t>::type); 

  for (int i=0; i<m_images.size(); i++)
  {
    int col     = i % images_per_row;
    int row     = floor(i / images_per_row); 

    img = m_images[i];

    roi = db_images(cv::Rect(col* img.cols, row*img.rows, img.cols, img.rows));
    img.convertTo(roi, cv::DataType<uint8_t>::type); 
  }

  IplImage ipl_img = db_images;
  pViewer->image(&ipl_img, 0, 0, db_images.cols-1, db_images.rows-1);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double
FaceIdDataBase::normpdf_similarity
  (
  double x,
  double& prob_same, 
  double& prob_diff
  )
{
  prob_same = normpdf_same(x);
  prob_diff = normpdf_diff(x);
  
  double sum  = prob_same + prob_diff;
  if (prob_same > prob_diff)
  {
    prob_same /= sum;
    prob_diff  = 1.0 - prob_same;
  }
  else
  {
    prob_diff /= sum;
    prob_same  = 1.0 - prob_diff;    
  } 
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double
FaceIdDataBase::normpdf_same
  (
  double x  
  )
{
  static double sigma  = STD_SAME_ID_SIMILARITY;
  static double sigma2 = STD_SAME_ID_SIMILARITY*STD_SAME_ID_SIMILARITY;
  static double log_sigma = std::log(STD_SAME_ID_SIMILARITY);
  double diff = x - MEAN_SAME_ID_SIMILARITY;
  
  return (1./(sigma*sqrt(M_PI)))*exp(-0.5*(((diff*diff)/sigma2)));
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
double
FaceIdDataBase::normpdf_diff
  (
  double x  
  )
{
  static double sigma  = STD_DIFFERENT_ID_SIMILARITY;
  static double sigma2 = STD_DIFFERENT_ID_SIMILARITY*STD_DIFFERENT_ID_SIMILARITY;
  static double log_sigma = std::log(STD_DIFFERENT_ID_SIMILARITY);
  double diff = x - MEAN_DIFFERENT_ID_SIMILARITY;
  
  return (1./(sigma*sqrt(2*M_PI)))*exp(-0.5*(((diff*diff)/sigma2)));
}
}; }; // namespace

