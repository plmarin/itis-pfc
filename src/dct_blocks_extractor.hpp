// -----------------------------------------------------------------------------
/**
 *  @brief DCT blocks image region feature extractor interface definition
 *  @author Jose M. Buenaposada
 *  @date 2011/06/09
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef DCT_BLOCKS_EXTRACTOR_HPP
#define DCT_BLOCKS_EXTRACTOR_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "face_area_image_extractor.hpp"
#include <boost/shared_ptr.hpp>
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <stdexcept>

namespace upm { namespace pcr
{
  
// ----------------------------------------------------------------------------
/*!
 *  @class EyesNotFound
 *  @brief 
 */
// ----------------------------------------------------------------------------
class EyesNotFoundException: public std::logic_error
{
public:
  EyesNotFoundException(const std::string& what_arg): logic_error(what_arg) {};
};

  
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to DCTBlocksExtractor type
 */
// -----------------------------------------------------------------------------
class DCTBlocksExtractor;
typedef boost::shared_ptr<DCTBlocksExtractor> DCTBlocksExtractorPtr;

// ----------------------------------------------------------------------------
/**
 * @class DCTBlocksExtractor
 * @brief An image region filter for equalization and masking
 *
 * See paper:
 *  J. Stallkamp, H. K. Ekenel, R. Stiefelhagen 
 *  Video-based Face Recognition on Real-World Data 
 *  Int. Conference on Computer Vision - ICCV'07, 
 *  Rio de Janeiro, Brasil, October 2007. 
 *
 */
// -----------------------------------------------------------------------------
class DCTBlocksExtractor: public FaceAreaImageExtractor
{
public:

  DCTBlocksExtractor
    (
//     bool use_eyes_location = true
    );

  /** Copy constructor */
  DCTBlocksExtractor
    (
    const DCTBlocksExtractor& filter
    );

  /** Asignment operator */
  DCTBlocksExtractor&
  operator =
    (
    const DCTBlocksExtractor& filter
    );

  ~DCTBlocksExtractor
    ();

  DCTBlocksExtractorPtr
  clone
    ();
    
  /**
   * @brief Crops the face region from the input image and extract DCT features by blocks
   *
   * @param pFrame Image comming from the camera.
   * @param f The face within pFrame to process.
   * @param pExtractedFeatures
   */
  virtual void
  operator () 
   (
   IplImage* pFrame,
   Face& f,
   CvMat* pExtractedFeatures
   ) throw(EyesNotFoundException);
   
  virtual size_t
  getFeatureVectorLength
    ();
   
private:

  void
  extractDCTBlocks
    (
    IplImage* pImage,
    CvMat* pFeatures
    );

  bool m_use_eyes_location;
};

}; }; // namespace

#endif
