// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of DCT features extractor by blocks. 
 *  @author Jose M. Buenaposada
 *  @date 2011/06/09
 *  @version $revision
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

#include "dct_blocks_extractor.hpp"
#include "similarity_warp_filter.hpp"
#include <opencv/cv.h>
#include <cmath>
#include "trace.hpp"

namespace upm { namespace pcr {

#define FACE_SIZE 32
#define SCALE (static_cast<double>(FACE_SIZE)/25.0)
#define NORM_LEFT_EYE_X cvRound(5.0*SCALE)
#define NORM_LEFT_EYE_Y cvRound(7.0*SCALE)
#define NORM_RIGHT_EYE_X cvRound(18.0*SCALE)
#define NORM_RIGHT_EYE_Y cvRound(7.0*SCALE)

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksExtractor::DCTBlocksExtractor 
  ( 
//   bool use_eyes_location /* = true */
  )
{
//   m_use_eyes_location = use_eyes_location;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksExtractor::DCTBlocksExtractor 
  ( 
  const DCTBlocksExtractor& extractor
  ) 
{
  (*this) = (extractor);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksExtractor& 
DCTBlocksExtractor::operator= 
  ( 
  const DCTBlocksExtractor& extractor
  )
{
  if (&extractor == this)
  {
    return (*this);
  };

//   m_use_eyes_location = extractor.m_use_eyes_location;
  
  return (*this);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksExtractor::~DCTBlocksExtractor()
{
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
DCTBlocksExtractorPtr
DCTBlocksExtractor::clone
  ()
{
  DCTBlocksExtractorPtr extractor_ptr(new DCTBlocksExtractor((*this)));

  return extractor_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
DCTBlocksExtractor::operator() 
  ( 
  IplImage* pFrame, 
  Face& f,
  CvMat* pExtractedFeatures
  ) throw(EyesNotFoundException)
{
  int x, y;
  int width, height;
  IplImage* pAuxImage;
  IplImage* pGrayImage;
  ImageRegion region;
  CvPoint left_eye, right_eye;
  CvPoint norm_face_left_eye;
  CvPoint norm_face_right_eye;
  double dist_src, dist_dst;
  double scale;
  double final_angle, init_angle;
    
  assert(pFrame != NULL);
  
  // Crop face area from pFrame image and resize it to the size of the pFaceImage.
  // If we have detected both eyes we use its location to make a similarity
  // transform.
  f.getRegion(region);
  region.getParams(x, y, width, height);
  f.getEyesRelativeCoordinates(left_eye, right_eye);

  assert(pExtractedFeatures != NULL);

  if ( (left_eye.x  == -1) || (left_eye.y  == -1) ||
       (right_eye.x == -1) || (right_eye.y == -1))
  {
    throw EyesNotFoundException("Eyes not found for DCT extraction");
  }
    
  norm_face_left_eye.x = NORM_LEFT_EYE_X;
  norm_face_left_eye.y = NORM_LEFT_EYE_Y;
  norm_face_right_eye.x = NORM_RIGHT_EYE_X;
  norm_face_right_eye.y = NORM_RIGHT_EYE_Y;
  SimilarityWarpFilter warpFilter(left_eye, right_eye, 
                                  norm_face_left_eye, norm_face_right_eye,
                                  cvSize(FACE_SIZE, FACE_SIZE));

  // We let the warpFilter to create the pAuxImage.
  pAuxImage = cvCreateImage(cvSize(FACE_SIZE, FACE_SIZE), pFrame->depth, pFrame->nChannels);
  warpFilter( pFrame, f, pAuxImage );
  
  assert(pAuxImage->width  == FACE_SIZE);
  assert(pAuxImage->height == FACE_SIZE);
  
  cvSaveImage("DCT_ALIGNED_IMAGE.png", pAuxImage);
  extractDCTBlocks(pAuxImage, pExtractedFeatures);
  cvSave("DCT_PER_BLOCKS.txt", pExtractedFeatures);
  
  // release memory
  cvReleaseImage(&pAuxImage);
};

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
DCTBlocksExtractor::extractDCTBlocks
  (
  IplImage* pImage,
  CvMat* pFeatures
  )
{
  IplImage* pDCTImage;
  IplImage* pGrayImage;
  IplImage* pGrayImageF;
  int i, j;
  int ind;
  double value;
  float *row;
  double dct_values[5];
  double dct_norm;
  int zigzag_x[5] = {1, 0, 0, 1, 2};
  int zigzag_y[5] = {0, 1, 2, 1, 0};
  
  assert(pFeatures != NULL);
  assert(pImage != NULL);
  assert(pFeatures->cols == 1);
  assert(pFeatures->rows == 5*pow(cvRound(static_cast<double>(FACE_SIZE)/8.0),2));
  
  pDCTImage   = cvCreateImage(cvSize(8,8), IPL_DEPTH_32F, 1);
  pGrayImageF = cvCreateImage(cvSize(pImage->width, pImage->height), IPL_DEPTH_32F, 1);
  pGrayImage  = cvCreateImage(cvSize(pImage->width, pImage->height), pImage->depth, 1);
  if (pImage->nChannels == 3)
  {
    cvCvtColor( pImage, pGrayImage, CV_RGB2GRAY );
  }
  else
  {
    cvCopy( pImage, pGrayImage );
  }

  cvConvert( pGrayImage, pGrayImageF );

  ind = 0;
  for (int y=0; y < cvRound(FACE_SIZE/8); y++)
  {
    for (int x=0; x < cvRound(FACE_SIZE/8); x++)
    {
      CvRect rect;
      rect.x      = x*8;
      rect.y      = y*8;
      rect.width  = 8;
      rect.height = 8;
      cvSetImageROI(pGrayImageF, rect);
      cvDCT(pGrayImageF, pDCTImage, 0);

      
      dct_norm = 0;
      
      // We ignore the first value and put the next 5 
      // values in zig-zag on the pDCTFeatures vector.
      for (int i=0; i<5; i++)
      {
        row           = &CV_IMAGE_ELEM( pDCTImage, float, zigzag_y[i], 0 );
        dct_values[i] = row[zigzag_x[i]];
        dct_norm     += pow(row[zigzag_x[i]],2);
      }
      dct_norm = sqrt(dct_norm);
      
      for (int i=0; i<5; i++)
      { 
	if (dct_norm > 0.0)
	{
          cvmSet(pFeatures, ind++, 0, dct_values[i]/dct_norm); 
	}
	else
	{
          cvmSet(pFeatures, ind++, 0, dct_values[i]); 
	}
      }
     
      cvResetImageROI(pGrayImageF);
    }
  }

  cvReleaseImage(&pGrayImage);
  cvReleaseImage(&pGrayImageF);
  cvReleaseImage(&pDCTImage);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
size_t
DCTBlocksExtractor::getFeatureVectorLength
  ()
{
  return 5*pow(cvRound(static_cast<double>(FACE_SIZE)/8.0),2);
}


}; }; // namespaces



