// -----------------------------------------------------------------------------
/**
 *  @brief Face implementation
 *  @author Jose M. Buenaposada
 *  @date 2009/11/30
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ----------------------- INCLUDES --------------------------------------------
#include "face.hpp"
#include "face_processor.hpp"
#include "trace.hpp"

namespace upm { namespace pcr
{

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
Face::processFaceRegion
  (
  IplImage *pFrame
  )
{
  if (m_processor_ptr.use_count() > 0)
  {
    m_processor_ptr->processFrame(pFrame, (*this));
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
Face::showResults
  (
  Viewer* pViewer,
  IplImage *pFrame
  )
{
  if (m_processor_ptr.use_count() > 0)
  {
    m_processor_ptr->showResults(pViewer, pFrame, (*this));
  };
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void 
Face::getAttributes
  (
  FaceAttributes& attributes
  )
{
  if (m_processor_ptr.use_count() > 0)
  {
    m_processor_ptr->getAttributes(attributes);
  };
};

}; }; // namespace
