
#include <opencv/cv.h>
#include "viewer.hpp"
#include "image_region.hpp"
#include "eyes_detector.hpp"
#include "trace.hpp"
#include <iostream>
#include <iomanip> // setprecision
#include <sstream>
#include <fstream>
#include "create_haarcascade_ojoi.hpp"
#include "create_haarcascade_ojod.hpp"


#define DETECTION_SCALE_STEP 1.2
#define DETECTION_MIN_NEIGHBORS 3
#define	DETECTION_MIN_FACE_SIZE 80
#define CASCADE_FILE_LEFT_EYE  "./haarcascades/ojoI.xml"
#define CASCADE_FILE_RIGHT_EYE "./haarcascades/ojoD.xml"

bool
ParseArguments
  (
  int argc,
  char* argv[],
  std::string& image_filename,
  std::string& output_filename,
  std::string& cascade_filename
  )
{
  bool return_value = false;

  if (argc == 1)
  {
    std::cout << std::endl;
    std::cout << "Usage: detect_eyes <path_to_image_file> <output_file_name> <haar_cascade_name>" << std::endl;
    std::cout << std::endl;
  }
  else if (argc == 2)
  {
    image_filename = argv[1];
    return_value =  true;
  }
  else if (argc == 3)
  {
    image_filename = argv[1];
    output_filename = argv[2];
    return_value =  true;
  }
  else if (argc == 4)
  {
    image_filename = argv[1];
    output_filename = argv[2];
    cascade_filename = argv[3];
    return_value = true;
  }

  return return_value;
}


int 
main
  ( 
  int argc, 
  char** argv 
  )
{
  float red_color[3]   = {1.0, 0.0, 0.0};
  CvHaarClassifierCascade* pCascade                 = NULL;
  CvHaarClassifierCascade* pCascadeLeftEye          = NULL;
  CvHaarClassifierCascade* pCascadeRightEye         = NULL;
  CvMemStorage* pStorage                               = NULL;
  IplImage *pFrame                                  = NULL;
  //double t;
 // int x, y, width, height;
  std::string cascade_filename = "haarcascade_frontalface_alt.xml"; //"haarcascade_frontalface_alt2.xml";
  std::string output_filename = "viola_jones_detection.txt";
  std::string image_filename;

  if (!ParseArguments(argc, argv, image_filename, output_filename, cascade_filename))
  {
    exit(1);
  }

  pCascade = (CvHaarClassifierCascade*)cvLoad( cascade_filename.c_str(), 0, 0, 0 );

  if (!pCascade)
  {
    TRACE_ERROR("ERROR: Could not load classifier cascade file: " << cascade_filename << std::endl);
    exit(1);
  }

  std::cout << "image_filename=" << image_filename << std::endl;
  std::cout << "output_filename=" << output_filename << std::endl;
  std::cout << "cascade_filename=" << cascade_filename << std::endl;

  // Create the cascade classifiers for the left and right eyes.
//  pCascadeLeftEye  = createHaarcascade_ojoI();
//  pCascadeRightEye = createHaarcascade_ojoD();

  pCascadeLeftEye  = (CvHaarClassifierCascade*)cvLoad( CASCADE_FILE_LEFT_EYE , 0, 0, 0 );
  if( !pCascadeLeftEye )
  {
    TRACE_ERROR("ERROR: Could not load left eye classifier cascade: ");
    TRACE_ERROR( CASCADE_FILE_LEFT_EYE << std::endl;);
    return -1;
  }

  pCascadeRightEye = (CvHaarClassifierCascade*)cvLoad( CASCADE_FILE_RIGHT_EYE , 0, 0, 0 );
  if( !pCascadeRightEye )
  {
    TRACE_ERROR("ERROR: Could not load right eye classifier cascade: ");
    TRACE_ERROR( CASCADE_FILE_RIGHT_EYE << std::endl;);
    return -1;
  }
  upm::pcr::EyesDetector eyes_detector(pCascadeLeftEye, pCascadeRightEye, pCascade->orig_window_size.width);

  std::ifstream input(image_filename.c_str());
  if (!input.is_open())
  {
    TRACE_ERROR("ERROR: Could not load image file: " << image_filename << std::endl);
    exit(1);
  }
  input.close();

  pFrame = cvLoadImage( image_filename.c_str(), 1 );
  pStorage = cvCreateMemStorage(0);

  std::ofstream out(output_filename.c_str());
  if (!out.is_open())
  {
    std::cerr << "ERROR: Could not open output file: " << output_filename << std::endl;
    cvReleaseImage( &pFrame );
    cvReleaseMemStorage( &pStorage );
    cvReleaseHaarClassifierCascade( &pCascade );
    cvReleaseHaarClassifierCascade( &pCascadeLeftEye );
    cvReleaseHaarClassifierCascade( &pCascadeRightEye );
    exit(1);
  }

  CvSeq* faces = cvHaarDetectObjects(pFrame, pCascade, pStorage,
                              DETECTION_SCALE_STEP,
                              DETECTION_MIN_NEIGHBORS,
                              CV_HAAR_DO_CANNY_PRUNING,
                              cvSize(DETECTION_MIN_FACE_SIZE, DETECTION_MIN_FACE_SIZE));


  for( int i = 0; i < (faces ? faces->total : 0); i++ )
  {
    CvRect* r   = (CvRect*)cvGetSeqElem( faces, i );
    upm::pcr::ImageRegion region;
    CvPoint left_eye, right_eye;

    left_eye.x  = -1;
    left_eye.y  = -1;
    right_eye.x = -1;
    right_eye.y = -1;

    region.setParams(r->x, r->y, r->width, r->height);

    // We find here the eyes of the detected face
    eyes_detector.detect(pFrame, region, left_eye, right_eye);

    out << r->x << " " << r->y << " " << r->width << " " << r->height; 
    out << " " << left_eye.x << " " << left_eye.y << " " << right_eye.x << " " << right_eye.y << std::endl;
  }
  out.close();

  cvReleaseImage( &pFrame );
  cvReleaseMemStorage( &pStorage );
  cvReleaseHaarClassifierCascade( &pCascade );
 // cvReleaseHaarClassifierCascade( &pCascadeLeftEye );
 // cvReleaseHaarClassifierCascade( &pCascadeRightEye );

  exit(0);
}

