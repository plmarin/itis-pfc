// -----------------------------------------------------------------------------
/**
 *  @brief FacesAnalyzer definition
 *  @author Jose M. Buenaposada
 *  @date 2009/13/10
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACES_ANALYZER_HPP
#define FACES_ANALYZER_HPP

// ----------------------- INCLUDES --------------------------------------------
#include "viewer.hpp"
#include "faces_tracker.hpp"
#include "face_processor.hpp"
#include <vector>
//#include "types_definitions.hpp"

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 * @class FacesAnalyzer
 * @brief A class that detects and tracks all faces on the video sequence frames.
 */
// -----------------------------------------------------------------------------
class FacesAnalyzer
{
public:
  
  FacesAnalyzer
    (
    FacesTrackerPtr faces_tracker_ptr,
    FaceProcessorPtr processor_prototype_ptr
    );

  ~FacesAnalyzer
    ();

  /**
   * @brief Detect, track and analyse faces on the image
   *
   * @param pImage an IplImage OpenCv image.
   */
  void 
  processFrame 
    (
    IplImage *pImage
    );

  /**
   * @brief Visualise results after processFrame call.
   *
   * @param pViewer The "Graphic Window" over to which draw the results.
   * @param pImage The last processed frame by a "processFrame" call.
   */
  void 
  showResults
    (
    Viewer* pViewer,
    IplImage *pImage
    );

  /**
   * @brief Gets the attributes of each tracked face and its image region.
   *
   * @param faces_attributes 
   * @param faces_image_region
   */
  void
  getTrackedFacesInfo
    (
    std::vector<FaceAttributes>& faces_attributes,
    std::vector<ImageRegion>& faces_image_regions
    );

private:

  FaceProcessorPtr m_processor_prototype_ptr;
  FacesTrackerPtr m_faces_tracker_ptr;
  Faces m_tracked_faces;
};

}; }; // namespace

#endif
