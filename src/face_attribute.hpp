// -----------------------------------------------------------------------------
/**
 *  @brief FaceAttribute interface definition.
 *  @author Jose M. Buenaposada
 *  @date 2011/01/28
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_ATTRIBUTE_HPP
#define FACE_ATTRIBUTE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <boost/shared_ptr.hpp>
#include <string>
#include <map> // multimap
#include <iostream>
#include <iomanip> // setprecision

namespace upm { namespace pcr
{
// ----------------------------------------------------------------------------
/**
 *  Definition of an smart pointer to FaceAttribute object
 */
// -----------------------------------------------------------------------------
class FaceAttribute;
typedef boost::shared_ptr<FaceAttribute> FaceAttributePtr;

// -----------------------------------------------------------------------------
/**
 *  Definition of a table of smart pointers to FaceAttribute. The 
 *  table is indexed by the attribute name field.
 */
// -----------------------------------------------------------------------------
typedef std::multimap<std::string, FaceAttributePtr> FaceAttributes;
typedef std::pair<std::string, FaceAttributePtr> FaceAttributePair;

// -----------------------------------------------------------------------------
/**
 * @class FaceAttribute
 * @brief Stores information about a face (detected on an image) attribute.
 */
// -----------------------------------------------------------------------------
class FaceAttribute
{
public:

  enum FaceAttributeFormat 
  {
   ATTR_FLOAT_TYPE, 
   ATTR_STRING_TYPE, 
   ATTR_INTERVAL_TYPE,
   ATTR_MEAN_STD_TYPE  
  };

  FaceAttribute
    (
    std::string name,
    FaceAttributeFormat format,
    std::string value,
    float confidence
    )
    : 
    m_name(name),
    m_format(format),
    m_value(value),
    m_confidence(confidence)
    {};

  ~FaceAttribute
    () {};

   void
   setParams
     (
     std::string name,
     FaceAttributeFormat format,
     std::string value,
     float confidence
     ) { m_name = name; m_format = format; m_value = value; m_confidence = confidence; };

   void
   getParams
     (
     std::string& name,
     FaceAttributeFormat& format,
     std::string& value,
     float& confidence
     ) { name = m_name; format = m_format; value = m_value; confidence = m_confidence; };

   std::string
   getName
     () { return m_name; };


protected:
  /**
   * Name of the attribute. For example:
   *
   *   "expression"
   */
  std::string m_name;

  /** 
   * This field will be one of the followings:
   *
   * ATTR_FLOAT_TYPE    e.g  "5.65"
   * ATTR_STRING_TYPE   e.g  "value"
   * ATTR_INTERVAL_TYPE e.g  "[5.5, 6.5]"
   * ATTR_MEAN_STD_TYPE e.g  "5.5+-6.5" 
   *
   * and will give the interpretation of the m_value field.
   */
   FaceAttributeFormat m_format;

  /**
   * An string with the value in the format given by m_format
   */
  std::string m_value;

  /**
   * The confidence in the attribute, 0.0 is low confidence 
   * and 1.0 is total confidence. 
   */
  float m_confidence; 
};


inline std::ostream& 
operator<< 
  (
  std::ostream& out,
  FaceAttribute& attribute
  ) 
{ 
  std::string name;
  std::string value; 
  float confidence;
  FaceAttribute::FaceAttributeFormat format;

  attribute.getParams(name, format, value, confidence); 
  out << "name=[" << name << "], value=[" << value << "], confidence= [" << std::setprecision(3) << confidence << "]"; 
  return out; 
};

}; }; // namespace

#endif
