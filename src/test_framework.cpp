
#include <opencv/cv.h>
#include "viewer.hpp"
#include "faces_analyzer.hpp"
//#include "biggest_face_tracker.hpp"
#include "all_faces_tracker.hpp"
#include "face_processors_group.hpp"
#include "face_id_lda_bgmodel_processor.hpp"
#include "face_id_database.hpp"
#include "face_background_model.hpp"
#include "squared_euclidean_distance.hpp"
#include "permutations_distance.hpp"
#include "trace.hpp"
#include <iostream>
#include <iomanip> // setprecision
#include <sstream>
#include "create_haarcascade_ojoi.hpp"
#include "create_haarcascade_ojod.hpp"

/*#include "pie_imagen_prueba.hpp"
// #include "lda_projection.hpp"
#include "lda_pca_projection.hpp"*/
#include <boost/program_options.hpp>

#define DETECTION_SCALE_STEP 1.2
#define DETECTION_MIN_NEIGHBORS 3
#define	DETECTION_MIN_FACE_SIZE 80
#define CASCADE_FILE_LEFT_EYE  "/usr/share/opencv/haarcascades/haarcascade_mcs_lefteye.xml"
#define CASCADE_FILE_RIGHT_EYE "/usr/share/opencv/haarcascades/haarcascade_mcs_righteye.xml"
#define CASCADE_FILE_FACE "/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml"
#define DRAW_EYES true

namespace po = boost::program_options;


int 
main
  ( 
  int argc, 
  char** argv 
  )
{
  float red_color[3]   = {1.0, 0.0, 0.0};
  CvHaarClassifierCascade* pCascade                 = NULL;
  CvHaarClassifierCascade* pCascadeLeftEye          = NULL;
  CvHaarClassifierCascade* pCascadeRightEye         = NULL;
  CvCapture* pCapture                               = NULL;
  IplImage *pFrame                                  = NULL;
  upm::pcr::Viewer* pViewer                         = NULL;
  upm::pcr::Viewer* pViewerDB                       = NULL;
  bool viewer_initialised                           = false;
  double t;
  std::vector<upm::pcr::FaceAttributes> faces_attributes;
  std::vector<upm::pcr::ImageRegion> faces_image_regions;
  int x, y, width, height;
  
  // Face identification framework related variables ...
//   upm::pcr::DistancePtr sq_euclidean_dist_ptr(dynamic_cast<upm::pcr::Distance* >(new upm::pcr::SquaredEuclideanDistance()));
  upm::pcr::DistancePtr perm_dist_ptr(dynamic_cast<upm::pcr::Distance* >(new upm::pcr::PermutationsDistance()));  
  upm::pcr::FaceBackgroundModelPtr bg_model_ptr(new upm::pcr::FaceBackgroundModel());
  upm::pcr::FaceIdDataBasePtr face_id_db_ptr(new upm::pcr::FaceIdDataBase(perm_dist_ptr));  


  std::string cascade_name;
  std::string input_name;
  std::string output_name;

  try
  {
    // --------------------------------------------------------------
    // Declare the supported program options.
    po::options_description desc("Allowed options");
    desc.add_options()
      ("help", "produce help message")
      ("cascade", po::value<std::string>(), "set the face cascade file")
      ("cascade-left-eye", po::value<std::string>(), "set the left eye cascade file")
      ("cascade-right-eye", po::value<std::string>(), "set the right eye cascade file")
      ("input-name", po::value<std::string>(), "set the source image source path.")
    ;

    po::positional_options_description p;
    p.add("input-name", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);    

    if (vm.count("help")) 
    {
        std::cout << desc << "\n";
        return 1;
    }  
  
    if (vm.count("cascade"))
    {
      TRACE_INFO(" Cascade name supplied ..." << std::endl;)
      cascade_name = vm["cascade"].as<std::string>();
    }
    else
    {
      cascade_name = CASCADE_FILE_FACE;
      TRACE_INFO(" Default face cascade name: " << cascade_name << std::endl;)
    }

    TRACE_INFO(" Loading cascade:  "  << cascade_name << std::endl;)
    pCascade = (CvHaarClassifierCascade*)cvLoad( cascade_name.c_str(), 0, 0, 0 );
    if( !pCascade )
    {
      TRACE_ERROR("ERROR: Could not load classifier cascade" << std::endl;)
      std::cout << desc << "\n";
      return 0;
    }
  
    if (vm.count("cascade-left-eye"))
    {
      cascade_name = vm["cascade-left-eye"].as<std::string>();
    }
    else
    {
      cascade_name = CASCADE_FILE_LEFT_EYE;
      TRACE_INFO(" Default left eye cascade name: " << cascade_name << std::endl;)
    }
  
    pCascadeLeftEye  = (CvHaarClassifierCascade*)cvLoad( cascade_name.c_str() , 0, 0, 0 );
    if ( !pCascadeLeftEye )
    {
      TRACE_ERROR("ERROR: Could not load left eye classifier cascade: ");
      TRACE_ERROR( cascade_name << std::endl;);
      return 0;
    }  

    if (vm.count("cascade-right-eye"))
    {
      cascade_name = vm["cascade-right-eye"].as<std::string>();
    }
    else
    {  
      cascade_name = CASCADE_FILE_RIGHT_EYE;
      TRACE_INFO("Default right eye cascade name: " << cascade_name << std::endl;)
    }

    pCascadeRightEye = (CvHaarClassifierCascade*)cvLoad( cascade_name.c_str() , 0, 0, 0 );
    if( !pCascadeRightEye )
    {
      TRACE_ERROR("ERROR: Could not load right eye classifier cascade: ");
      TRACE_ERROR( cascade_name << std::endl;);
      return 0;
    }

    TRACE_INFO("Cascades created ..." << std::endl;)

    if (vm.count("input-name"))
    {
      input_name = vm["input-name"].as<std::string>();
    }
    
    TRACE_INFO("Input file name: "  << input_name << std::endl;)
   
  }
  catch(std::exception& e)  
  {
    std::cout << e.what() << "\n";
    return 1;
  }    
  
  if( input_name.empty() || (isdigit(input_name.c_str()[0]) && input_name.c_str()[1] == '\0') )
  {
    TRACE_INFO("Capturing from camera  ..." << std::endl;)
    pCapture = cvCaptureFromCAM( input_name.empty() ? 0 : input_name.c_str()[0] );
  }
  else
  {
    TRACE_INFO("Capturing from AVI file ..." << std::endl;)
    pCapture = cvCaptureFromAVI( input_name.c_str() ); 
  }

  if ( !pCapture )
  {
    TRACE_ERROR("ERROR: Could not grab images" << std::endl;)
    return -1;
  }

/*
  upm::pcr::FacesTrackerPtr faces_tracker_ptr(dynamic_cast<upm::pcr::FacesTracker* >(new upm::pcr::BiggestFaceTracker(pCascade,
                               pCascadeLeftEye,
                               pCascadeRightEye,
                               DETECTION_SCALE_STEP,
                               DETECTION_MIN_NEIGHBORS,
                               DETECTION_MIN_FACE_SIZE)));
*/

  upm::pcr::FacesTrackerPtr faces_tracker_ptr(dynamic_cast<upm::pcr::FacesTracker* >(new upm::pcr::AllFacesTracker(pCascade,
                               pCascadeLeftEye,
                               pCascadeRightEye,
                               DETECTION_SCALE_STEP,
                               DETECTION_MIN_NEIGHBORS,
                               DETECTION_MIN_FACE_SIZE,
                               DRAW_EYES)));

  // Create face processor example
  upm::pcr::FaceProcessorPtr face_id_processor_ptr(dynamic_cast<upm::pcr::FaceProcessor*>(new upm::pcr::FaceIdLDABgModelProcessor(bg_model_ptr, face_id_db_ptr)));

  // Create the face processors group with expressions and age estimation ...
//  upm::pcr::FaceProcessorsGroup* pGroup = new upm::pcr::FaceProcessorsGroup();
//  pGroup->addProcessor(face_id_processor_ptr);
//  upm::pcr::FaceProcessorPtr processor_group_ptr(dynamic_cast<upm::pcr::FaceProcessor*>(pGroup));
  
  // Add the processors group to the face analyzer
  //upm::pcr::FacesAnalyzer faces_analyzer(faces_tracker_ptr, processor_group_ptr);
  upm::pcr::FacesAnalyzer faces_analyzer(faces_tracker_ptr, face_id_processor_ptr);

  pViewer = new upm::pcr::Viewer();
  pViewerDB = new upm::pcr::Viewer();

  for(;;)
  {
    if ( !cvGrabFrame( pCapture ))
    {
      break;
    }

    pFrame = cvRetrieveFrame( pCapture );

    if ( !pFrame )
    {
      break;
    }

    if (!viewer_initialised)
    {
      //pViewer->init(pFrame->width, pFrame->height, "Age estimation");
      pViewer->init(pFrame->width, pFrame->height, "Reconocimiento facial");
      pViewerDB->init(pFrame->width, pFrame->height, "Face Data Base");
      viewer_initialised = true;
    }

    // Process frame
    t = static_cast<double>(cvGetTickCount());
    faces_analyzer.processFrame(pFrame);
    t = static_cast<double>(cvGetTickCount()) - t;
    std::ostringstream outs;  // Declare an output string stream.
    outs << "FPS=" << std::setprecision(3) << (static_cast<double>(cvGetTickFrequency())*1000.*1000.)/t << std::ends;
    std::string fps_info = outs.str();      // Get the created string from the output stream.

    // Drawing results
    pViewer->beginDrawing();
    pViewer->image(pFrame, 0, 0, pFrame->width, pFrame->height);
    faces_analyzer.showResults(pViewer, pFrame);
    pViewer->text(fps_info, 20, pFrame->height-20, red_color, 0.5);
    pViewer->endDrawing();

    pViewerDB->beginDrawing();
    face_id_db_ptr->showDataBaseImages(pViewerDB, 10);  
    pViewerDB->endDrawing();

    // Get tracked faces info:
//     faces_analyzer.getTrackedFacesInfo(faces_attributes, faces_image_regions);
//     TRACE_INFO("================== NEW FRAME ================" << std::endl;)
//     for (int i=0; i < faces_image_regions.size(); i++)
//     {
//       faces_image_regions[i].getParams(x, y, width, height);
//       TRACE_INFO("Location Face [" << i << "] => x=" << x << ", y=" << y << ", width=" << width << ", height=" << height << std::endl;)
//       TRACE_INFO("Attributes Face [" << i << "] => " << std::endl;)
// 
//       upm::pcr::FaceAttributes::iterator it = faces_attributes[i].begin();
//       for ( ; it != faces_attributes[i].end(); it++)
//       {
//         TRACE_INFO((*(*it).second) << std::endl);
//       }
//     };
//     TRACE_INFO("================== END FRAME ================" << std::endl;)
  }
  cvReleaseCapture( &pCapture );
  delete pViewer;

  return 0;
}

