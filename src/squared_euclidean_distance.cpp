// -----------------------------------------------------------------------------
/**
 *  @brief Squared eucludean distance implementation
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "squared_euclidean_distance.hpp"

namespace upm { namespace pcr
{

SquaredEuclideanDistance::SquaredEuclideanDistance
  () {};

SquaredEuclideanDistance::~SquaredEuclideanDistance
  () {};

double
SquaredEuclideanDistance::computeDistance
  (
  CvMat* d1,
  CvMat* d2
  )
{
  return computeDistance(cv::Mat(d1,true),cv::Mat(d2,true));
}

double
SquaredEuclideanDistance::computeDistance
  (
  cv::Mat d1,
  cv::Mat d2
  )
{
  cv::Mat res = d1 * d2;
  return static_cast<double>(res.data[0]);
}

}; }; // namespace

