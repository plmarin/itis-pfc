// -----------------------------------------------------------------------------
/**
 *  @brief Distance abstract class between OpenCV vectors
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef DISTANCE_HPP
#define DISTANCE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>
#include <boost/shared_ptr.hpp>

namespace upm { namespace pcr
{
  
class Distance;
typedef boost::shared_ptr<Distance> DistancePtr;

// ----------------------------------------------------------------------------
/**
 * @class Distance
 * @brief Abastract base class for distances between OpenCV Vectors
 * 
 */
// -----------------------------------------------------------------------------
class Distance
{
public:
  Distance
    () {};

  virtual 
  ~Distance
    () {}

  /**
   * @brief Get the distance between descriptors (OpenCV Vectors)
   *
   * @param descriptor1 
   * @param descriptor2 
   */
  virtual double
  computeDistance
   (
   CvMat* d1,
   CvMat* d2
   ) = 0;
};

}; }; // namespace

#endif

