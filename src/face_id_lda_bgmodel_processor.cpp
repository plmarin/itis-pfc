// -----------------------------------------------------------------------------
/**
 *  @brief Implementation of face processor for recognition with LDA and BG model.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/04
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------
#include "face_id_lda_bgmodel_processor.hpp"
#include "face.hpp"
#include <sstream>
#include "trace.hpp"
//#include "lda_projection.hpp"
//#include "lda_pca_projection.hpp"
#include "pca_lda_pie_model.h" // Changed by JMBUENA.

namespace upm { namespace pcr
{

const int FACE_CROPPED_WIDTH                  = IMG_WIDTH; //64;
const int FACE_CROPPED_HEIGHT                 = IMG_HEIGHT; //64;
const float DETECTION_WINDOW_REDUCTION_TOP    = 0.20;
const float DETECTION_WINDOW_REDUCTION_LEFT   = 0.15;
const float DETECTION_WINDOW_REDUCTION_RIGHT  = 0.15;
const float DETECTION_WINDOW_REDUCTION_BOTTOM = 0.1;
  
// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdLDABgModelProcessor::FaceIdLDABgModelProcessor
  (
  FaceBackgroundModelPtr face_bg_model,  
  FaceIdDataBasePtr face_id_db
  ):
  m_face_id_db(face_id_db),
  m_face_bg_model(face_bg_model)
{
  m_image = cv::Mat::zeros(FACE_CROPPED_WIDTH, FACE_CROPPED_HEIGHT, cv::DataType<uint8_t>::type); 
  
  m_P    = cv::Mat(ROWS_PCA_LDA_P_MATRIX, COLS_PCA_LDA_P_MATRIX, cv::DataType<float>::type, PCA_LDA_P_MATRIX);
  m_mean = cv::Mat(ROWS_PCA_LDA_MEAN_MATRIX, COLS_PCA_LDA_MEAN_MATRIX, cv::DataType<float>::type, PCA_LDA_MEAN_MATRIX);
  
  m_face_id = FaceIdDataBase::UNKNOWN_ID;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Copy constructor
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdLDABgModelProcessor::FaceIdLDABgModelProcessor
  (
  const FaceIdLDABgModelProcessor& processor
  )
{
  (*this)           = (processor);
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  Asignment operator.
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdLDABgModelProcessor&
FaceIdLDABgModelProcessor::operator =
  (
  const FaceIdLDABgModelProcessor& processor
  )
{   
  if (&processor == this)
  {
    return (*this);

  };

  m_image  = processor.m_image.clone();
  
  // Remove the face descriptors seen so far ...
  while (m_face_descriptors_seen.size() > 0)
  {
    m_face_descriptors_seen.pop();
  }  

  // Add the processor face descriptors seen so far to this 
  // object ...
  while (m_face_descriptors_seen.size()> 0)
  {
    m_face_descriptors_seen.pop();
  }

  m_face_id       = processor.m_face_id;
  m_face_bg_model = processor.m_face_bg_model;
  m_face_id_db    = processor.m_face_id_db;

  // Those are only a reference to the constant data matrix.
  m_P    = processor.m_P;
  m_mean = processor.m_mean;

  return (*this);  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method:  
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceIdLDABgModelProcessor::~FaceIdLDABgModelProcessor
  ()
{  
  // Remove the face descriptors seen so far ...
  while (m_face_descriptors_seen.size() > 0)
  {
    m_face_descriptors_seen.pop();
  }  
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
FaceProcessorPtr
FaceIdLDABgModelProcessor::clone
  ()
{
  FaceProcessorPtr processor_ptr(dynamic_cast<FaceProcessor*>(new FaceIdLDABgModelProcessor((*this))));

  return processor_ptr;
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdLDABgModelProcessor::processFrame 
 (
 IplImage* pFrame,
 Face& f
 )
{
  ImageRegion region;
  int x, y, width, height;
  int x_crop, y_crop, width_crop, height_crop;
  cv::Mat aux_image;
  
  f.getRegion(region);
  region.getParams(x, y, width, height);

  x_crop      = x + round(width*DETECTION_WINDOW_REDUCTION_LEFT);
  y_crop      = y + round(height*DETECTION_WINDOW_REDUCTION_TOP);
  height_crop = round(height * (1.0 - (DETECTION_WINDOW_REDUCTION_TOP+DETECTION_WINDOW_REDUCTION_BOTTOM)));
  width_crop  = round(width * (1.0 - (DETECTION_WINDOW_REDUCTION_LEFT+DETECTION_WINDOW_REDUCTION_RIGHT)));

  //------------------------------------------------------------------
  // Crop the face from the input image.
  cv::Mat frame(pFrame);
  cv::Mat roi(frame, cv::Rect(x_crop, y_crop, width_crop, height_crop)); 
  cv::resize(roi, aux_image, cv::Size(FACE_CROPPED_WIDTH, FACE_CROPPED_HEIGHT));
  
  // Convert the cropped image to gray scale
  if (frame.channels() == 3)
  {
    cvtColor( aux_image, m_image, CV_BGR2GRAY );
  }
  else
  {
    m_image = aux_image.clone();
  }
  
  //------------------------------------------------------------------
  // Project the input image vector over the LDA model subspace.
  cv::Mat image_vector; 
  m_image.reshape(1, m_image.rows*m_image.cols).convertTo(image_vector, m_mean.type());
  cv::Mat lda = m_P * (image_vector - m_mean);

  //------------------------------------------------------------------
  // Compute the highlevel descriptor using the BackgroundModel class
  cv::Mat descriptor = m_face_bg_model->computeHighLevelDescriptor(lda);   

  //------------------------------------------------------------------
  // Compute the face id if we have enough images
  m_face_descriptors_seen.push(descriptor);

  if (m_face_descriptors_seen.size() >= MINIMAL_NUMBER_OF_IMAGES)
  {
    m_face_id = m_face_id_db->getID(m_face_descriptors_seen, m_image.clone());
    m_face_descriptors_seen.pop();
  }
}

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdLDABgModelProcessor::showResults
  (
  Viewer* pViewer,
  IplImage* pFrame,
  Face& f
  )
{
  int x_window, y_window;
  int window_width, window_height;
  float color[] = {1.0, 1.0, 1.0};
  float black[] = {0.0, 0.0, 0.0};
  std::ostringstream out;
  ImageRegion region;

  // Get the image region from the face.
  f.getRegion(region);

  region.getParams(x_window, y_window, window_width, window_height);

  // Plot ID:
  int rectangle_height      = floor(static_cast<double>(window_width)*0.2);
  int plot_area_border      = floor(static_cast<double>(rectangle_height)*0.15);
  int plot_area_left        = x_window; // + window_width + plot_area_border;

  pViewer->filled_rectangle(x_window, y_window - rectangle_height,
                            window_width, rectangle_height, black);

  out << std::setprecision(3);
  out << "Id = "<< FaceIdDataBase::faceIdToString(m_face_id);
  pViewer->text(out.str(), 
                x_window, 
                y_window - plot_area_border, 
                color, 
                static_cast<double>(window_width)/250.0,
                1);

  
  // Plot cropped image.
  IplImage ipl_image = m_image;
  pViewer->image(&ipl_image, x_window + window_width, 
                 y_window + 20, window_width*0.5, window_width*0.5);
};

// -----------------------------------------------------------------------------
//
// Purpose and Method: 
// Inputs:
// Outputs:
// Dependencies:
// Restrictions and Caveats:
//
// -----------------------------------------------------------------------------
void
FaceIdLDABgModelProcessor::getAttributes
  (
  FaceAttributes& attributes
  )
{
  bool expression_attr_found = false;
  std::string attr_name      = "face_id";

  // Male probability
  FaceAttributePtr attribute_face_id_ptr(new FaceAttribute(attr_name, 
                                 FaceAttribute::ATTR_STRING_TYPE,
                                 FaceIdDataBase::faceIdToString(m_face_id),
                                 m_face_id_confidence));
  attributes.insert(std::make_pair(attr_name, attribute_face_id_ptr));
};


}; }; // namespace

