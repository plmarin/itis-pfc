// -----------------------------------------------------------------------------
/**
 *  @brief Face Background Model to compute Distances to faces in a Fixed DB.
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef FACE_BACKGROUND_MODEL_HPP
#define FACE_BACKGROUND_MODEL_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>
#include "distance.hpp"
#include <boost/shared_ptr.hpp>

namespace upm { namespace pcr
{

class FaceBackgroundModel;
typedef boost::shared_ptr<FaceBackgroundModel> FaceBackgroundModelPtr;  

typedef boost::shared_ptr<cv::flann::Index> KdTreePtr;  
  
// ----------------------------------------------------------------------------
/**
 * @class FaceBackgroundModel
 * @brief Computes how similar is an input face to a set of predefined faces (background model)
 * 
 * In this class we have stored a set of low level descriptors of the background 
 * model faces (for example the images of 68 subjects in PIE Database). We 
 * distance of a given image low level descriptor with all the individuals in the
 * Background model and we get the high level descriptor for the input face. 
 */
// -----------------------------------------------------------------------------
class FaceBackgroundModel
{
public:

  FaceBackgroundModel
    ();
    
  ~FaceBackgroundModel
    ();

  /**
   * @brief Compute the subjects distances based high level descriptor
   */
  cv::Mat
  computeHighLevelDescriptor
    (
    cv::Mat low_level_descriptor
    );
    
  std::vector<int>
  calcularTau
    (
    std::vector<int>&
    );
   
private:
//   cv::flann::KDTreeIndexParams m_indexParams;
  std::vector<KdTreePtr> m_subject_kdtrees;
};

}; }; // namespace

#endif

