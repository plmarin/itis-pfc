// -----------------------------------------------------------------------------
/**
 *  @brief Distance between permutations vectors
 *  @author Jose M. Buenaposada, Pablo Marín
 *  @date 2012/4
 *  @version $revision$
 *
 *  $id$
 *
 *  Grupo de investigación en Percepción Computacional y Robótica)
 *  (Perception for Computers & Robots research Group)
 *  Facultad de Informática (Computer Science School)
 *  Universidad Politécnica de Madrid (UPM) (Madrid Technical University)
 *  http://www.dia.fi.upm.es/~pcr
 *
 */
// -----------------------------------------------------------------------------

// ------------------ RECURSION PROTECTION -------------------------------------
#ifndef PERMUTATIONS_DISTANCE_HPP
#define PERMUTATIONS_DISTANCE_HPP

// ----------------------- INCLUDES --------------------------------------------
#include <opencv/cv.h>
#include <opencv/ml.h>

namespace upm { namespace pcr
{

// ----------------------------------------------------------------------------
/**
 * @class PermutationsDistance
 * @brief Distance (inverse similarity) between permutations vectors
 * 
 */
// -----------------------------------------------------------------------------
class PermutationsDistance
{
public:
  PermutationsDistance
    ();

  virtual 
  ~PermutationsDistance
    ();

  /**
   * @brief Get the distance between descriptors (OpenCV Vectors)
   *
   * @param descriptor1 
   * @param descriptor2 
   */
  virtual double
  computeDistance
   (
   cv::Mat d1,
   cv::Mat d2
   );
};

}; }; // namespace

#endif

