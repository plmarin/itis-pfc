# itis-pfc
(Ingeniería Técnica en Informática de Sistemas - Proyecto Final de Carrera)
Face recognition


Estudio de "La identificación en vídeo".

La idea de este estudio es identificar a las personas que se vayan viendo de frente en un vídeo. Esa persona puede ser que sea una persona vista con anterioridad o una que no haya salido todavía en el vídeo, por lo que el sistema que se pretende desarrollar en este proyecto debe de poder identificar a esa persona y tomar la decisión correcta: ¿Es una nueva persona o, por el contrario, ya la había visto antes?

Para ello, se va a trabajar con dos bases de datos (PIE y LFW) las cuales ayudarán en el proceso para identificar a todos aquellos individuos que pasen por el escenario. También se hará uso de algunos algoritmos que ya han dado buenos resultados en otros estudios relacionados con este tema y que ayudarán a reducir la dimensionalidad del problema (PCA y LDA).

El objetivo propuesto es el de crear un sistema que sea capaz de identificar a los individuos usando todo lo que se ha contado anteriormente.

